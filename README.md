# WSO2 API Manager deployment with WSO2 API Manager Analytics

1. Clone WSO2 API Manager Docker git repository.
   ```
   git clone https://gitlab.com/docker.public/wso2am.git
   ```
   
2. Switch to `2.2.0` folder.
   ```
   cd 2.2.0/
   ```
   
3. Execute following Docker command to start the deployment.
   ```
   docker-compose up -d
   ```

4. Once the deployment is started, try to access the web UIs via following URLs and default credentials <br> 
   on your favorite web browser.

   ```
   https://localhost:9443/publisher
   https://localhost:9443/store
   https://localhost:9443/admin
   https://localhost:9443/carbon
   ```
   Access the servers using following credentials.
   
   * Username: admin <br>
   * Password: admin