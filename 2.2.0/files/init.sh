#!/bin/sh
# ------------------------------------------------------------------------
# ------------------------------------------------------------------------
set -e

sed -i 's/<!--HostName>.*<\/HostName-->/<HostName>localhost<\/HostName>/g' ${WSO2_SERVER_HOME}/repository/conf/carbon.xml
sed -i 's/<!--MgtHostName>.*<\/MgtHostName-->/<MgtHostName>localhost<\/MgtHostName>/g' ${WSO2_SERVER_HOME}/repository/conf/carbon.xml
sed -i 's/<GatewayEndpoint>http:\/\/${carbon.local.ip}:${http.nio.port},https:\/\/${carbon.local.ip}:${https.nio.port}<\/GatewayEndpoint>/<GatewayEndpoint>http:\/\/localhost:${http.nio.port},https:\/\/localhost:${https.nio.port}<\/GatewayEndpoint>/g' ${WSO2_SERVER_HOME}/repository/conf/api-manager.xml
sed -i 's/<ReceiverUrlGroup>tcp:\/\/${carbon.local.ip}:${receiver.url.port}<\/ReceiverUrlGroup>/<ReceiverUrlGroup>tcp:\/\/localhost:${receiver.url.port}<\/ReceiverUrlGroup>/g' ${WSO2_SERVER_HOME}/repository/conf/api-manager.xml
sed -i 's/<AuthUrlGroup>ssl:\/\/${carbon.local.ip}:${auth.url.port}<\/AuthUrlGroup>/<AuthUrlGroup>ssl:\/\/localhost:${auth.url.port}<\/AuthUrlGroup>/g' ${WSO2_SERVER_HOME}/repository/conf/api-manager.xml
sed -i 's/<ServiceURL>tcp:\/\/${carbon.local.ip}:${jms.port}<\/ServiceURL>/<ServiceURL>tcp:\/\/localhost:${jms.port}<\/ServiceURL>/g' ${WSO2_SERVER_HOME}/repository/conf/api-manager.xml

# start the WSO2 Carbon server
sh ${WSO2_SERVER_HOME}/bin/wso2server.sh